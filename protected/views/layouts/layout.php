<!DOCTYPE html>
<html>
<head>
	<title>Токарные резцы и пластины</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="/img/favicon.ico">

	<link rel="stylesheet" type="text/css" href="/css/reset.css">
	<link rel="stylesheet" type="text/css" href="/fonts/fonts.css">
	<link rel="stylesheet" type="text/css" href="/css/style.css?1">
	<link rel="stylesheet" type="text/css" href="/js/libs/lightbox/css/lightbox.min.css">
	
	<link rel="stylesheet" type="text/css" href="/perfetto/perfetto.css">
	<script src="/perfetto/perfetto.js"></script>

	<script type="text/javascript" src="/js/dist/libs.js?v=1"></script>	
	<script type="text/javascript" src="/js/dist/build.js?v=1"></script>
</head>
<body>
	
	<div class="mobile-menu">
		<ul class="mobile-menu__list">
			<li class="mobile-menu__list-item">
				<a href="/" class="mobile-menu__list-item-link">Главная</a>
			</li>
			<li class="mobile-menu__list-item">
				<a class="mobile-menu__list-item-link">Каталог</a>
				<ul class="mobile-menu__sublist">
					<li class="mobile-menu__sublist-item">
						<?php $url ='/catalog/tokarnie-rezci' ?>
						<a href="<?php echo $url?>" class="mobile-menu__sublist-item-link">Токарные резцы</a>
					</li>
					<li class="mobile-menu__sublist-item">
						<?php $url ='/catalog/tokarnie-plastini' ?>
						<a href="<?php echo $url?>" class="mobile-menu__sublist-item-link">Токарные пластины</a>
					</li>
				</ul>
			</li>
			<li class="mobile-menu__list-item">
				<?php $url =Yii::app()->createUrl('main/contacts') ?>
				<a href="<?php echo $url?>" class="mobile-menu__list-item-link">Контакты</a>
			</li>
		</ul>
	</div>
	<div class="mobile-menu-btn" onclick="window.mMenu.show()"></div>
	<div class="sitewrap">
		<div class="sitewrap__inner">
			<header class="header">
				<div class="header__inner">
					<a class="header__logo" href="/">
						<img class="header__logo-img" src="/img/logo.png">
					</a>
					<nav class="header__menu">
						<ul class="header__menu-items">
							<li class="header__menu-item">
								<?php if ($this->activeMenuLink == "index") $linkClass="header__menu-item-link_active"; else $linkClass="";?>
								<a class="header__menu-item-link <?php echo $linkClass?>" href="/" >Главная</a>
							</li>
							<li class="header__menu-item">
								<?php if ($this->activeMenuLink == "catalog") $linkClass="header__menu-item-link_active"; else $linkClass="";?>
								<span class="header__menu-item-link <?php echo $linkClass?>" href="">Каталог продукции</span>
								<ul class="header__menu-sub">
									<li class="header__menu-sub-item">
										<?php $url ='/catalog/tokarnie-rezci' ?>
										<a class="header__menu-sub-item-link" href="<?php echo $url?>">Токарные резцы</a>
									</li>
									<li class="header__menu-sub-item">
										<?php $url ='/catalog/tokarnie-plastini' ?>
										<a class="header__menu-sub-item-link" href="<?php echo $url?>">Токарные пластины</a>
									</li>
									<li class="header__menu-sub-bottom"></li>
								</ul>
							</li>	
							<li class="header__menu-item">
								<?php if ($this->activeMenuLink == "contacts") $linkClass="header__menu-item-link_active"; else $linkClass="";?>
								<?php $url =Yii::app()->createUrl('main/contacts') ?>
								<a class="header__menu-item-link <?php echo $linkClass?>" href="<?php echo $url?>">Контакты</a>
							</li>	
						</ul>
					</nav>
					<div class="header__contacts">
						<div class="header__contacts-line">
							<a class="header__contacts-phone" href="tel:8-000-000-00-00">8-950-749-34-64</a>
							<a class="header__contacts-email" href="mailto:info@tokrez.ru">info@tokrez.ru</a>
						</div>
						<div class="header__contacts-line">
							<div class="header__contacts-address">г.Челябинск, ул. 40 лет Октября, 33</div>
						</div>
						<div class="header__contacts-line">
							<div class="header__contacts-delivery">Доставка во все регионы России</div>
						</div>
					</div>
					<div class="header__recall">
						<div class="header__recall-title">Есть вопросы?</div>
						<div class="header__recall-text">позвоните нам или закажите</div>
						<div class="header__recall-btn">Обратный звонок</div>
					</div>				
				</div>
			</header>
				<?php echo $content ?>
			<footer class="footer">
				<div class="footer__inner">
					<div class="footer__logo">
						<img class="footer__logo-img" src="/img/logo.png">
						<div class="footer__logo-year">2017г.</div>
					</div>
					<div class="footer__info">
						<div class="footer__delivery">Доставка во все регионы России</div>
						<div class="footer__contacts">
							<div class="footer__contacts-line">
								<a class="footer__contacts-phone" href="tel:8-000-000-00-00">8-950-749-34-64</a>
								<a class="footer__contacts-email" href="mailto:info@tokrez.ru">info@tokrez.ru</a>
							</div>
							<div class="footer__contacts-line">
								<div class="footer__contacts-address">г.Челябинск, ул. 40 лет Октября, 33</div>
							</div>
						</div>
					</div>
					
				</div>
			</footer>
		</div>
	</div>
	
	<div class="order-popup">
		<div class="order-popup__inputs">
			<div class="order-popup__prod">
				<div class="order-popup__prod-img-wr">	
					<img src="" class="order-popup__prod-img">
				</div>			
				<div class="order-popup__prod-name productName"></div>
			</div>
			<div class="order-popup__inp formWrapName">
				<div class="order-popup__inp-label">Имя <span class="order-popup__inp-req formError">обязательное поле</span></div>
				<input type="text" class="order-popup__inp-val formInput">
			</div>
			<div class="order-popup__inp formWrapPhone">
				<div class="order-popup__inp-label">Телефон <span class="order-popup__inp-req formError">обязательное поле</span></div>
				<input type="text" class="order-popup__inp-val formInput">
			</div>
			<div class="order-popup__inp formWrapEmail">
				<div class="order-popup__inp-label">Email <span class="order-popup__inp-req formError">обязательное поле</span></div>
				<input type="text" class="order-popup__inp-val formInput">
			</div>
			<div class="order-popup__inp">
				<div class="order-popup__sendBtn formSendBtn">Отправить</div>
			</div>
		</div>
		<div class="order-popup__sending">
			<img src="/img/loading.gif" class="order-popup__sending-img">
		</div>
		<div class="order-popup__success">
			<div class="order-popup__success-text">Сообщение отправлено!</div>
			<div class="order-popup__closeBtn" onclick="window.orderPopup.close()">Закрыть</div>
		</div>
		<div class="order-popup__fail">
			<div class="order-popup__fail-text">Произошла ошибка. Сообщение не доставлено.</div>
			<div class="order-popup__closeBtn" onclick="window.orderPopup.close()">Закрыть</div>
		</div>		
	</div>
	<div class="order-popup__bg" onclick="window.orderPopup.close()"></div>
	
</body>
</html>