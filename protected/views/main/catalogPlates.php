<div class="catalog catalog_plates">
	<div class="catalog__title"><?php echo $catalogTitle; ?></div>
	<div class="catalog__table-wrap">
		<table id="catalog-table" class="catalog-table">
			<thead class="catalog-table__thead">
				<tr class="catalog-table__tr catalog-table__tr_head">
					<th class="catalog-table__th catalog-table__th-name">Наименование</th>
					<th class="catalog-table__th catalog-table__th-size">Размер&nbsp;&nbsp;&nbsp;&nbsp;</th>
					<th class="catalog-table__th catalog-table__th-country">Производитель&nbsp;&nbsp;&nbsp;&nbsp;</th>
					<th class="catalog-table__th catalog-table__th-comment">Примечание&nbsp;&nbsp;&nbsp;&nbsp;</th>
					<th class="catalog-table__th catalog-table__th-price">Цена&nbsp;&nbsp;&nbsp;&nbsp;</th>
					<th class="catalog-table__th catalog-table__th-stock">Наличие&nbsp;&nbsp;&nbsp;&nbsp;</th>
					<th class="catalog-table__th catalog-table__th-img"></th>
					<th></th>
				</tr>
			</thead>
			<tbody class="catalog-table__tbody">	
				<?php foreach ($products as $product): ?>			
					<tr class="catalog-table__tr catalog-table__tr_body">
						<td class="catalog-table__td">
							<div class="catalog-table__item-name" ><?php echo $product->name ?></div>
						</td>
						<td class="catalog-table__td">
							<div class="catalog-table__item-size" ><?php echo $product->size ?></div>
						</td>
						<td class="catalog-table__td">
							<div class="catalog-table__item-country" ><?php echo $product->country ?></div>
						</td>
						<td class="catalog-table__td">
							<div class="catalog-table__item-comment" ><?php echo $product->comment ?></div>
						</td>
						<td class="catalog-table__td">
							<div class="catalog-table__item-price" ><?php echo $product->price ?></div>
						</td>
						<td class="catalog-table__td">
							<div class="catalog-table__item-stock">
								<?php if ($product->stock==0):?>
									нет в наличии
								<?php else: ?>
									в наличии
								<?php endif; ?>
							</div>
						</td>
						<td class="catalog-table__td catalog-table__td-img">
							<div class="catalog-table__item-img-wrap">
								<a href="<?php echo $product->photo1Url ?>" data-lightbox="prod<?php echo $product->id ?>" data-title="<?php echo $product->name ?>">
									<img src="<?php echo $product->photo1ThmbUrl ?>" class="catalog-table__item-img1">
								</a>								
							</div>						
						</td>
						<td class="catalog-table__td">
							<div class="catalog-table__orderBtn" onclick="window.orderPopup.show('<?php echo $product->name ?>','<?php echo $product->photo1ThmbUrl ?>')">заказать</div>
						</td>
					</tr>
				<? endforeach; ?>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	window.catalogSearch = <?php echo CJavaScript::encode($search)?>;
</script>