<?php
class Import 
{
	private $fileName;
	private $filePath;

	const ROW_DELIMITER = ";";
	const FIRST_ROW = 1;

	const REZ_NAME_CLM = 0;
	const REZ_DERJAVKA_CLM = 1;
	const REZ_PLASTINA_CLM = 2;
	const REZ_PRICE_CLM = 3;
	const REZ_STOCK_CLM = 4;
	const REZ_PHOTO1_CLM = 5;
	const REZ_PHOTO1_THMB_CLM = 6;
	const REZ_PHOTO2_CLM = 7;
	const REZ_PHOTO2_THMB_CLM = 8;
	const REZ_PHOTO3_CLM = 9;

	const PLT_NAME_CLM = 0;
	const PLT_SIZE_CLM = 1;
	const PLT_COUNTRY_CLM = 2;
	const PLT_COMMENT_CLM = 3;
	const PLT_PRICE_CLM = 4;
	const PLT_STOCK_CLM = 5;
	const PLT_PHOTO1_CLM = 6;
	const PLT_PHOTO1_THMB_CLM = 7;
	

	const STOCK_YES = "в наличии";

	function __construct(){
		
	}

	private function createFilePath($fileName) {
		if ($fileName==null) {
			$this->fileName = "import.csv";
		}
		else {
			$this->fileName = $fileName;
		}
		$ds = DIRECTORY_SEPARATOR;
		$this->filePath = Yii::app()->basePath.$ds.'import'.$ds.$this->fileName;
	}

	public function cutters($fileName=null) {
		$this->createFilePath($fileName);
		Cutters::model()->deleteAll();
		$rows = file($this->filePath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
		for ($i=self::FIRST_ROW;$i<=(count($rows)-self::FIRST_ROW);$i++) {
			$row = $rows[$i];
			$rowItems = explode(self::ROW_DELIMITER, $row);
			$cutter = new Cutters;
			$cutter->name = $rowItems[self::REZ_NAME_CLM];
			$cutter->derjavka = $rowItems[self::REZ_DERJAVKA_CLM];
			$cutter->plastina = $rowItems[self::REZ_PLASTINA_CLM];
			$cutter->price = $rowItems[self::REZ_PRICE_CLM];


			if ($rowItems[self::REZ_STOCK_CLM]==self::STOCK_YES) {
				$cutter->stock = 1;
			}
			else {
				$cutter->stock = 0;
			}
			
			$cutter->photo1 = $rowItems[self::REZ_PHOTO1_CLM];
			$cutter->photo1_thmb = $rowItems[self::REZ_PHOTO1_THMB_CLM];
			$cutter->photo2 = $rowItems[self::REZ_PHOTO2_CLM];
			$cutter->photo2_thmb = $rowItems[self::REZ_PHOTO2_THMB_CLM];
			$cutter->photo3 = $rowItems[self::REZ_PHOTO3_CLM];
			$cutter->save();
			$errors = $cutter->getErrors();
			if (count($errors)!=0) {
				$error = implode(';',array_keys($errors));
				throw new ImportException($i,$error);				
			}
		}
	}

	public function plates($fileName=null) {
		$this->createFilePath($fileName);
		Plates::model()->deleteAll();
		$rows = file($this->filePath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
		for ($i=self::FIRST_ROW;$i<=(count($rows)-self::FIRST_ROW);$i++) {
			$row = $rows[$i];
			$rowItems = explode(self::ROW_DELIMITER, $row);
			$plate = new Plates;
			$plate->name = $rowItems[self::PLT_NAME_CLM];
			$plate->size = $rowItems[self::PLT_SIZE_CLM];
			$plate->country = $rowItems[self::PLT_COUNTRY_CLM];
			$plate->comment = $rowItems[self::PLT_COMMENT_CLM];
			$plate->price = $rowItems[self::PLT_PRICE_CLM];

			if ($rowItems[self::PLT_STOCK_CLM]==self::STOCK_YES) {
				$plate->stock = 1;
			}
			else {
				$plate->stock = 0;
			}
			
			$plate->photo1 = $rowItems[self::PLT_PHOTO1_CLM];
			$plate->photo1_thmb = $rowItems[self::PLT_PHOTO1_THMB_CLM];
			$plate->save();
			$errors = $plate->getErrors();
			if (count($errors)!=0) {
				$error = implode(';',array_keys($errors));
				throw new ImportException($i,$error);				
			}
		}
	}

}