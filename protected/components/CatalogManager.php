<?php
class CatalogManager 
{
	const CUTTERS_IMG_FOLDER_URL = "/catalog-imgs/cutters/";
	const PLATES_IMG_FOLDER_URL = "/catalog-imgs/plates/";

	public function getCutters($limit=null){
		$criteria = new CDbCriteria;
		if ($limit!=null) {
			$criteria->limit = $limit;	
		}		
		$cutters = Cutters::model()->findAll($criteria);
		return $cutters;
	}

	public function getPlates($limit=null){
		$criteria = new CDbCriteria;
		if ($limit!=null) {
			$criteria->limit = $limit;	
		}		
		$plates = Plates::model()->findAll($criteria);
		return $plates;
	}
}